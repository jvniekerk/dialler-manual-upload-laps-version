﻿Option Explicit On
Imports System.Data.SqlClient

Module modSqlConnections
    Function GetConnectionString() As String
        'Dim hostname = System.Net.Dns.GetHostName()
        'Dim ip As String = ""
        'Dim connString As String = "Initial Catalog=DiallerStagingArea;Data Source="

        'For Each hostAdr In System.Net.Dns.GetHostEntry(hostname).AddressList()
        '    ip = hostAdr.ToString

        '    If ip = "172.27.63.23" Or ip = "172.27.63.17" Then
        '        Return "Initial Catalog=DiallerStagingArea;Data Source=UB-SQLCLSTR-1;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
        '        '    connString += "UB-SQLCLSTR-1"
        '        'Else
        '        '    connString += "172.27.63.23"
        '    End If
        'Next hostAdr

        'connString += ";User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"

        'Return connString


        'Return "Initial Catalog=DiallerStagingArea;Data Source=172.27.63.23;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"

        Dim dataSource = "ub-avgr01.unclebuck.ukfast"

        Dim user = "DiallerManualListsUpload"
        Dim pw = "p099898!""!j[oUY"

        'Dim user = "DiallerStagingArea"
        'Dim pw = "8GG&67f6!£sar2"

        ''''Dim user = "Integrator"
        'Dim user = "Integrator2"
        'Dim pw = "ss%$W£$%09-0-!"

        'Dim user = "TTCCalculator"
        'Dim pw = "0097*^&56564""ol"

        'Dim user = "HubspotDataAPIIntegration"
        'Dim pw = "hf476%^$%3536$^^!"

        'Dim user = "unclebuck\jvniekerk"
        'Dim pw = "B00m4p13"

        'Dim dataSource = "172.27.63.23"
        'Dim user = "sa"
        'Dim pw = "u3zD2Ix6"

        Dim connString = "Initial Catalog=DiallerStagingArea;Data Source=" & dataSource & ";User ID=" & user & ";Password=" & pw & ";Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"

        'MsgBox(connString)

        Return connString
    End Function

    Public Function ConnectToSql(ByVal sqlQuery As String, Optional ByVal download As Boolean = True, Optional ByVal table As Boolean = False) As String
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader
        Dim results As String = ""

        myConn = New SqlConnection(GetConnectionString())
        myCmd = myConn.CreateCommand
        myCmd.CommandTimeout = 0
        myCmd.CommandText = sqlQuery
        myConn.Open()
        myReader = myCmd.ExecuteReader()

        If download Then
            results = XMLString(myReader)
        Else
            results = ReturnValue(myReader)
        End If

        myReader.Close()
        myReader = Nothing
        myConn.Close()
        myConn = Nothing

        Return results
    End Function
End Module
