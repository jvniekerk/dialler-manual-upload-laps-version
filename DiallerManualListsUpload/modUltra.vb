﻿Module modUltra
    Public Function UltraUpload(ByVal request As String) As String
        Dim upload As New net.ultraasp.client.upload.Service
        Return upload.UploadDataByUserString("unclebuck", "Int_ManualUpload", "Unc13Buck", request)
    End Function

    Public Function UltraDownload(ByVal request As String) As String
        Dim download As New net.ultraasp.client.download.Service
        Return download.DownloadDataByUserString("unclebuck", "Int_ManualUpload", "Unc13Buck", request)
    End Function

    Function UploadLists(ByVal query As String) As Boolean
        Dim dt As DataTable = sqlToDataTable(query)
        Dim listsTable As DataTable = dt.DefaultView.ToTable(True, "ListID")
        Dim thisListID As Integer
        Dim thisTable As DataTable
        Dim xml As String
        Dim schema As String
        Dim maxRows As Integer = 500
        Dim success As Boolean = False

        'Try
        For Each row As DataRow In listsTable.Rows
                thisListID = row.Item("ListID")
                'MsgBox(thisListID)
                schema = GetSchema(thisListID)
                thisTable = FilterDataTable(dt, thisListID)
                Dim rows As Integer = thisTable.Rows.Count
            'MsgBox("rows" & " -> " & rows.ToString)

            If rows <= maxRows Then
                    xml = BuildXML(schema, thisListID, thisTable)
                'MsgBox(xml)
                'Call UltraUpload(xml)
                Dim response As String = UltraUpload(xml)
                'MsgBox(response)
            Else
                    Dim thisRows As Integer = maxRows
                'MsgBox("thisRows" & " -> " & thisRows.ToString)
                Dim smallTable As DataTable = thisTable.Clone

                    While thisTable.Rows.Count > 0
                    'MsgBox("rows left -> " & thisTable.Rows.Count.ToString)
                    If thisTable.Rows.Count < maxRows Then thisRows = thisTable.Rows.Count

                        'For i As Integer = 0 To thisRows - 1
                        '    smallTable.ImportRow(thisTable.Rows(i))
                        'Next i

                        'For i As Integer = 0 To thisRows - 1
                        '    thisTable.Rows(0).Delete()
                        'Next i

                        For i As Integer = 0 To thisRows - 1
                            smallTable.ImportRow(thisTable.Rows(0))
                            thisTable.Rows(0).Delete()
                        Next i

                        xml = BuildXML(schema, thisListID, smallTable)
                    'MsgBox(xml)
                    'Call UltraUpload(xml)
                    Dim response As String = UltraUpload(xml)
                    'MsgBox(response)

                    smallTable.Rows.Clear()
                    End While
                End If
            Next row

            success = True
        'Catch ex As Exception
        '    success = False
        'End Try

        Return success
    End Function

    Sub getLists()
        Dim request As String = "<data clientid='10864'><System.BaseIndex /></data>"
        Dim response As String = UltraDownload(request)
        Dim rows As Integer = getRowNumbers(response)
        Dim listID As Integer
        Dim listName As Integer
        Dim schemaName As Integer

        If rows > 0 Then
            Dim thisArray As String(,) = GetArrayFromXMLwithHeaders(response)
            Dim cols As Integer = thisArray.GetLength(1)
            rows = thisArray.GetLength(0)

            For i As Integer = 0 To cols - 1
                If thisArray(0, i) = "BaseIndexId" Then
                    listID = i
                ElseIf thisArray(0, i) = "BaseName" Then
                    listName = i
                ElseIf thisArray(0, i) = "SchemaCode" Then
                    schemaName = i
                End If
            Next

            Dim query As String = " insert into DiallerStagingArea.dbo.UltraDownloadXML (DownloadType, DownloadText, DownloadDate, [Rows]) "
            query = query & " select 'Lists', '" & response & "', getdate(), " & rows & " "
            query = query & Environment.NewLine & " delete from DiallerStagingArea.dbo.Lists "
            query = query & Environment.NewLine & " insert into DiallerStagingArea.dbo.Lists (ListID, ListName, SchemaCode) values "
            For i As Integer = 1 To rows - 1
                query = query & Environment.NewLine & "(" & thisArray(i, listID) & ", '" & thisArray(i, listName) & "', '" &
                    thisArray(i, schemaName) & "') "

                If i < rows - 1 Then
                    query = query & ", "
                End If
            Next i

            query = query & Environment.NewLine & "update DiallerStagingArea.dbo.Lists set Stream = case "
            query = query & "when ListName like '%Arrears Stream%' then 'Arrears Stream' "
            query = query & "when ListName like '%PreFund Stream%' then 'PreFund Stream' else 'Other' end "
            Call ConnectToSql(query, False)
        End If
    End Sub

    Function GetSchema(ByVal listID As String) As String
        If listID < "169" Then
            Select Case listID
                Case "155"
                    Return "NewIntegrationTest_13Feb17"
                Case "167"
                    Return "NewIntegrationTest_13Feb17"
                Case "168"
                    Return "NewIntegrationTest_13Feb17"
                Case Else
                    Return ""
            End Select
        Else
            If listID = 235 Then
                Return "StagingAreaIntegrationV2"
            Else
                Return "StagingAreaIntegration"
            End If
        End If
    End Function
End Module
