﻿Option Explicit On
Imports System.Data.SqlClient

Module modFunctions

    Sub RunListUpload(ByVal query As String)
        If MsgBox("Have you deleted the List Records in the list(s) you want to upload to?", vbYesNo + vbQuestion, "Check Lists Cleared") = 6 Then
            MsgBox("Please wait while the upload is run. This could take some time...")
            Call UploadLists(query)

            If MsgBox("Upload complete. Would you like to upload another list?", vbYesNo + vbQuestion, "Upload Complete") <> 6 Then
                Application.Exit()
            End If
        Else
            MsgBox("Closing Application so you can go delete list records before reloading accounts", vbOKOnly + vbCritical, "Please delete old accounts from list")
            Application.Exit()
        End If
    End Sub

    Function BuildXML(ByVal schema As String, ByVal listID As String, ByVal thisTable As DataTable) As String
        Dim xml As String = "<upload clientid='10864'><" & schema & " BaseIndexId='" & listID & "'><rows>"
        xml = xml & XMLfromDataTable(thisTable, "NewUpload")
        xml = xml & "</rows></" & schema & "></upload>"
        'MsgBox(xml)

        Return xml
    End Function

    Function getRowNumbers(response As String) As Integer
        Dim rows As Integer = 0
        rows = response.IndexOf("rows=") + Len("rows=") + 1
        response = Right(response, Len(response) - rows)
        rows = Left(response, response.IndexOf(""""))
        Return rows
    End Function

    Function GetArrayFromXMLwithHeaders(ByVal xmlString As String) As String(,)
        Dim thisArray As String(,)
        Dim rows As Integer = getRowNumbers(xmlString)
        Dim workingText As String
        Dim cols As Integer = 0
        Dim colName As String = ""

        If rows > 0 Then
            xmlString = Right(xmlString, Len(xmlString) - Len(Left(xmlString, xmlString.IndexOf("<row "))))
            workingText = Left(xmlString, xmlString.IndexOf("/>") + 2).Replace("<row ", "")
            cols = System.Text.RegularExpressions.Regex.Split(workingText, "=""").Length - 1
            ReDim thisArray(rows, cols)

            For i As Integer = 0 To cols - 1
                colName = Left(workingText, workingText.IndexOf("="""))
                thisArray(0, i) = colName
                colName = colName & "="""
                workingText = workingText.Replace(colName, "")
                colName = Left(workingText, workingText.IndexOf("""") + 2)
                workingText = Right(workingText, Len(workingText) - Len(colName))
            Next i


            For i As Integer = 1 To rows
                workingText = Left(xmlString, xmlString.IndexOf("/>") + 2)
                xmlString = xmlString.Replace(workingText, "")
                For j As Integer = 0 To cols - 1
                    thisArray(i, j) = GetValueFromXML(workingText, thisArray(0, j))
                Next j
            Next i
        Else
            ReDim thisArray(0, 0)
        End If

        Return thisArray
    End Function

    Function GetValueFromXML(ByVal xmlString As String, ByVal searchValue As String) As String
        searchValue = searchValue & "="""
        Dim startPos As Integer = xmlString.IndexOf(searchValue) + Len(searchValue)
        xmlString = Right(xmlString, Len(xmlString) - startPos)
        Dim endPos As Integer = xmlString.IndexOf("""")

        Return Left(xmlString, endPos)
    End Function

    Public Function XMLString(ByVal myReader As SqlDataReader) As String
        Dim results As String = ""
        Dim fields As Integer = myReader.FieldCount - 1
        Dim thisString As String = ""

        Do While myReader.Read()
            With myReader
                results = results & "<insertOnly"
                For k As Integer = 0 To fields
                    thisString = .GetString(k)
                    results = results & " " & .GetName(k) & "=""" &
                        Replace(Replace(Replace(Replace(thisString, ">", "&gt;"), "<", "&lt;"), "&", "&amp;"), "%", "&#37;") & """"
                Next
                results = results & "/>"
            End With
        Loop

        myReader.Close()
        Return results
    End Function

    Public Function ReturnValue(ByVal myReader As SqlDataReader) As String
        Dim results As String = ""
        Dim fields As Integer = myReader.FieldCount - 1
        Dim thisString As String = ""

        Do While myReader.Read()
            With myReader
                For k As Integer = 0 To fields
                    If k > 1 Then
                        results = " " & results & .GetString(k)
                    Else
                        results = results & .GetString(k)
                    End If
                Next
            End With
        Loop

        myReader.Close()
        Return results
    End Function

    Function FilterDataTable(ByVal startTable As DataTable, ByVal listID As String) As DataTable
        Dim listFilter As String = "ListID = '" & listID & "'"
        Dim view As DataView = New DataView(startTable, listFilter, "ListID", DataViewRowState.CurrentRows)
        Dim endTable As DataTable = view.ToTable()
        endTable.Columns.Remove("ListID")
        view = Nothing
        Return endTable
    End Function

    Function XMLfromDataTable(ByVal dataTable As DataTable, ByVal criteria As String) As String
        Dim xmlString As String = ""
        Dim i As Integer = 0
        Dim columns As Integer = dataTable.Columns.Count

        For Each row As DataRow In dataTable.Rows
            If criteria = "NewUpload" Then
                xmlString = xmlString & "<insertOnly"
            Else
                xmlString = xmlString & "<updateOnly"
            End If

            i = 0
            For Each item In row.ItemArray
                xmlString = xmlString & " " & dataTable.Columns(i).ColumnName & "= """ & item.ToString & """"
                i = i + 1
            Next item

            xmlString = xmlString & "/>"
        Next row

        Return xmlString
    End Function

    Function sqlToDataTable(ByVal sqlQuery As String) As DataTable
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader
        Dim connString = GetConnectionString()
        'MsgBox(connString)
        Dim results As New DataTable

        myConn = New SqlConnection(connString)
        myCmd = myConn.CreateCommand
        myCmd.CommandTimeout = 0
        myCmd.CommandText = sqlQuery

        Try
            myConn.Open()

            myReader = myCmd.ExecuteReader()

            results.Load(myReader)

            myReader.Close()
            myReader = Nothing
            myConn.Close()
            myConn = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return results
    End Function
End Module
