﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnUploadAll = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnUploadSelected = New System.Windows.Forms.Button()
        Me.lblName = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnUploadAll
        '
        Me.btnUploadAll.Location = New System.Drawing.Point(12, 113)
        Me.btnUploadAll.Name = "btnUploadAll"
        Me.btnUploadAll.Size = New System.Drawing.Size(119, 67)
        Me.btnUploadAll.TabIndex = 2
        Me.btnUploadAll.Text = "Upload ALL Dialler Lists"
        Me.btnUploadAll.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(321, 113)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(119, 67)
        Me.btnExit.TabIndex = 0
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnUploadSelected
        '
        Me.btnUploadSelected.Location = New System.Drawing.Point(167, 113)
        Me.btnUploadSelected.Name = "btnUploadSelected"
        Me.btnUploadSelected.Size = New System.Drawing.Size(119, 67)
        Me.btnUploadSelected.TabIndex = 1
        Me.btnUploadSelected.Text = "Upload SELECTED Dialler Lists"
        Me.btnUploadSelected.UseVisualStyleBackColor = True
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Calibri", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(12, 48)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(862, 46)
        Me.lblName.TabIndex = 3
        Me.lblName.Text = "Uncle Buck Finance LLP - Dialler Manual List Uploader"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(16.0!, 39.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(452, 232)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.btnUploadSelected)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnUploadAll)
        Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Form1"
        Me.Text = "Dialler Manual Lists Upload"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnUploadAll As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents btnUploadSelected As Button
    Friend WithEvents lblName As Label
End Class
