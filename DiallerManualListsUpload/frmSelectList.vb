﻿
Public Class frmSelectList
    Private Sub btnReturn_Click(sender As Object, e As EventArgs) Handles btnReturn.Click
        Close()

        Form1.Show()
    End Sub

    Private Sub frmSelectList_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sql As String = "exec Dialler.dbo.sp_Manual_GetLists"
        Dim dt As DataTable = sqlToDataTable(sql)

        If dt.Rows.Count Then
            For Each dr As DataRow In dt.Rows
                With cmbLists
                    .DataSource = dt
                    .DisplayMember = "List"
                    .ValueMember = "List"
                End With
            Next dr
        Else
            MsgBox("No lists to display")

            Close()

            Form1.Show()
        End If
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim listID As String = StrReverse(cmbLists.SelectedValue.ToString)

        listID = StrReverse(Microsoft.VisualBasic.Left(listID, listID.IndexOf(" "))).ToString
        'MsgBox(listID)


        Dim query As String = "exec Dialler.dbo.sp_Manual_Populate_ListsUpload @listNr = " & listID.ToString

        Call RunListUpload(query)
        'MsgBox("got data")
    End Sub
End Class