﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmbLists = New System.Windows.Forms.ComboBox()
        Me.lblLists = New System.Windows.Forms.Label()
        Me.btnReturn = New System.Windows.Forms.Button()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.lblName = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cmbLists
        '
        Me.cmbLists.FormattingEnabled = True
        Me.cmbLists.Location = New System.Drawing.Point(12, 117)
        Me.cmbLists.Name = "cmbLists"
        Me.cmbLists.Size = New System.Drawing.Size(497, 47)
        Me.cmbLists.TabIndex = 0
        '
        'lblLists
        '
        Me.lblLists.AutoSize = True
        Me.lblLists.Location = New System.Drawing.Point(12, 75)
        Me.lblLists.Name = "lblLists"
        Me.lblLists.Size = New System.Drawing.Size(428, 39)
        Me.lblLists.TabIndex = 1
        Me.lblLists.Text = "Please select a list to upload to:"
        '
        'btnReturn
        '
        Me.btnReturn.Location = New System.Drawing.Point(50, 170)
        Me.btnReturn.Name = "btnReturn"
        Me.btnReturn.Size = New System.Drawing.Size(150, 63)
        Me.btnReturn.TabIndex = 2
        Me.btnReturn.Text = "Return"
        Me.btnReturn.UseVisualStyleBackColor = True
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(315, 170)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(150, 63)
        Me.btnRun.TabIndex = 3
        Me.btnRun.Text = "Upload Selected List"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Calibri", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(12, 23)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(862, 46)
        Me.lblName.TabIndex = 4
        Me.lblName.Text = "Uncle Buck Finance LLP - Dialler Manual List Uploader"
        '
        'frmSelectList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(16.0!, 39.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(545, 264)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.btnReturn)
        Me.Controls.Add(Me.lblLists)
        Me.Controls.Add(Me.cmbLists)
        Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "frmSelectList"
        Me.Text = "Select a List to Upload To"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmbLists As ComboBox
    Friend WithEvents lblLists As Label
    Friend WithEvents btnReturn As Button
    Friend WithEvents btnRun As Button
    Friend WithEvents lblName As Label
End Class
